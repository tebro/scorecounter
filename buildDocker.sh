#!/bin/bash

NAME=${1:-scorecounter}

npm run build
docker build -t $NAME .

echo "Container built, named: $NAME"
