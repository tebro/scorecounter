import React, { Component } from 'react';
import './Game.css';


class Game extends Component {

    render(){
        return (
            <div>
                <ul>
                    {this.props.players.map((player, index) => {
                        return <PlayerButton player={player} key={index} updatePlayerScore={this.props.updatePlayerScore} index={index}/>
                    })}
                </ul>
            </div>
        )
    }
}

class PlayerButton extends Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(e){
        e.preventDefault();
        //var btn = e.target.closest('button');
        this.props.updatePlayerScore(this.props.index);
    }
    render(){
        return(
            <button className="Game-PlayerButton" onClick={this.handleClick}>
                <span className="Game-PlayerButton-Name">{this.props.player.name}</span>
                <span className="Game-PlayerButton-Score">{this.props.player.score}</span>
            </button>
        )
    }
}

export default Game;
