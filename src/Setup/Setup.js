import React, { Component } from 'react';
import './Setup.css';

class Setup extends Component {
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e) {
        e.preventDefault();
        var value = document.getElementById('Setup-Add-Player-Name-Field').value;
        this.props.addPlayer(value);
    }
    render(){
        return (
            <div>
                <div id="Setup-Add-Player">
                    <form onSubmit={this.handleSubmit}>
                        <input placeholder="Player name" id="Setup-Add-Player-Name-Field"></input>
                        <input type="submit"></input>
                    </form>
                </div>
                <div id="Setup-Player-List">

                </div>
            </div>
        )
    }
}

export default Setup;
