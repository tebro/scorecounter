import React, { Component } from 'react';
import Setup from './Setup/Setup.js';
import Game from './Game/Game.js';
import logo from './logo.svg';
import './App.css';

class App extends Component {
    constructor(){
        super();
        this.state = {players: []};
        this.addPlayer = this.addPlayer.bind(this);
        this.updatePlayerScore = this.updatePlayerScore.bind(this);
    }
    addPlayer(name){
        var players = this.state.players;
        players.push({
            name: name,
            score: 0,
        });
        this.setState({players: players});
    }
    updatePlayerScore(index){
        var players = this.state.players;
        players[index].score++;
        this.setState({players: players});
    }
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Scorecounter</h2>
                </div>
                <div>
                    <Setup addPlayer={this.addPlayer}/>
                    <Game players={this.state.players} updatePlayerScore={this.updatePlayerScore}/>
                </div>
            </div>
        );
    }
}

export default App;
